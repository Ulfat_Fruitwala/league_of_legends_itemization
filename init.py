from riotwatcher import LolWatcher, ApiError
import json, items

# Variables
# See README to generate personal API Key
# Enclose both api_key and userID with ' '
api_key = #'Personal API Key'# 
userID = #'Your Summoner ID'#

watcher = LolWatcher(api_key)
my_region = 'na1'

# User's Information
me = watcher.summoner.by_name(my_region, userID)
encryptedID = me['id']

# User's Game Status
gamestatus = watcher.spectator.by_summoner(my_region, encryptedID)


# Game variables
game_id = None
map_id = None
game_mode = None
game_type = None
game_queue_id = None
participants = None
team_id = None

# Lists and Dictionaries
user_team = []
enemy_team = []

user_team_tags = {}
enemy_team_tags = {}
enemy_tags_count = {}

# Game champion data
champ_data_new = {}

# Initialize game champion data
def champion_data_init():
	global champ_data_new
	
	# Open item.json from DataDragon, assuming it is in C drive
	# Please match ddragon version
	with open("C:/ddragon/11.6.1/data/en_US/champion.json", encoding="utf8") as f:
		data = json.load(f)
	champ_data = data['data']

	# Create a new dictionary with champion's key value as key
	for data in champ_data:
		key = int(champ_data[data]['key'])
		champ_data_new[key] = champ_data[data]

# Initialize user's game information
def game_init():
	global game_id, map_id, game_mode, game_type
	global game_queue_id, participants, team_id

	game_id = gamestatus['gameId']
	map_id = gamestatus['mapId']
	game_mode = gamestatus['gameMode']
	game_type = gamestatus['gameType']
	#game_queue_id = gamestatus["gameQueueConfigId"]
	participants = gamestatus['participants']

# Distinguish teammates and enemy team by their teamID
def init_champs():
	global user_team, enemy_team, participants

	participants_length = len(participants)
	user_team_id = participants[0]['teamId']
	enemy_team_id = participants[participants_length-1]['teamId']

	for x in participants:
		if x['teamId'] == user_team_id:
			user_team.append(x['championId'])
		else:
			enemy_team.append(x['championId'])

# Add champions to their respective team lists
def get_champions():
	global user_team, enemy_team
	temp1 = {} 
	temp2 = {}

	for teammate, enemy in zip(user_team, enemy_team):
		team_champ = champ_data_new.get(teammate)
		enemy_champ = champ_data_new.get(enemy)
		temp1[team_champ['id']] = team_champ
		temp2[enemy_champ['id']] = enemy_champ

	# Overwrite user and enemy teams to new dictionaries
	user_team = temp1
	enemy_team = temp2

# Add enemy team's statistics to a list "enemy_stats"
def set_tags():
	global user_team_tags, enemy_team_tags

	for teammate, enemy in zip(user_team, enemy_team):
		user_team_tags[teammate] = user_team[teammate]['tags']
		enemy_team_tags[enemy] = enemy_team[enemy]['tags']



# Determine type of champions on enemy team, e.g. Mages, Fighters...
def get_damage():
	global enemy_tags_count

	for champion in enemy_team_tags:
		tag = enemy_team_tags[champion]
		for tags in tag:
			if tags in enemy_tags_count:
				enemy_tags_count[tags] += 1
			else:
				enemy_tags_count[tags] = 1

def get_items():
	items.init(enemy_tags_count)

# Helper print function
def print_participants():
	print("Enemy team members: ")
	for x in user_team:
		print(x)
	for y in enemy_team:
		print(y)

# Helper print function
def print_enemy_tags():
	for x in enemy_team_tags:
		print(enemy_team_tags[x])

# Helper print function
def print_init():
	print("game id: "), print(game_id)
	print("map id: "), print(map_id)
	print("game mode: "), print(game_mode)
	print("game type: "), print(game_type)
	print("participants: "), print(participants)

def print_user_champion():
	for x in user_champion:
		print(x, user_champion[x])


def main_init():
	game_init()
	champion_data_init()
	init_champs()
	get_champions()
	set_tags()
	get_damage()

	get_items()


main_init()