# Itemization #
----------------
This application is developed to list suitable items to buy whilst playing Riot Game's League of Legends. After entering into the loading screen of the game, user can run the application to see what type of champions the enemy team has. In a decreasing order, the application will list relevant items to buy for each type of champion such as Mages, Tanks, Assasins, etc...

## Files
--------
Files associated with the program

- Code files: init.py, item.py
- Data files from Data Dragon: champion.json, item.json
- Download Data Dragon from --> https://developer.riotgames.com/docs/lol#data-dragon
  Latest version -->https://ddragon.leagueoflegends.com/cdn/dragontail-11.6.1.tgz
- Additional files: README

## Usage
--------
1. Sign up for a developer's account with Riot Games at: https://developer.riotgames.com/
2. Read through the documentation and Riot's policies associated with API key usage
3. Generate your personal API key (Don't share this with anyone)
4. Copy and paste the key within single quotes in file init.py at line #7
5. Place your Summoner ID within single quotes in file init.py at line #8
6. Save file
7. From command line interface run: python init.py

## Built Using
--------------
- Python, Json

## Author
---------
- Ulfat Fruitwala 