# Get Items Module
import operator, json

item_data = {}
assasin_bool = 0

def init_items():
	global item_data

	# Open item.json from DataDragon, assuming it is in C drive
	# Please match ddragon version
	with open("C:/ddragon/11.6.1/data/en_US/item.json", encoding="utf8") as file:
		data = json.load(file)
	item_data = data['data']
	list_item = list(item_data.keys())

	# Save Muramana, special case
	muramana = item_data['3042']

	# Remove boots, consumables, mythic items
	# Keep only Legendary items
	for x in list_item:
		i = item_data[x]
		tag = item_data[x]['tags']
		if 'Boots' in tag or 'Consumable' in tag:
			item_data.pop(x, None)

		elif 'into' in i.keys():
			item_data.pop(x, None)

		elif 'depth' not in i.keys():
			item_data.pop(x, None)

		elif 'Mythic Passive' in item_data[x]['description']:
			item_data.pop(x, None)

	item_data['3042'] = muramana

# Switch Functions
def assassin():
	global assasin_bool

	if(item_data):
		print("To counter attack damage champions, consider buying:")
		for index in list(item_data.keys()):
			if 'Armor' in item_data[index]['tags']:
				print(item_data[index]['name'], ': ', item_data[index]['plaintext'])
				item_data.pop(index, None)
		assasin_bool = 1
	else:
		return

def fighter():
	if assasin_bool == 0:
		assassin()
	else:
		return

def mage():
	if(item_data):
		print("To counter mages, consider buying:")
		for index in list(item_data.keys()):
			if 'Magic Resist' in item_data[index]['tags']:
				print(item_data[index]['name'], ': ', item_data[index]['plaintext'])
				item_data.pop(index, None)
	else:
		return

def marksman():
	if assasin_bool == false:
		assassin()
	else:
		return

def support():
	return

def tank():
	if(item_data):
		print("To counter tanks, consider buying:")
		for index in list(item_data.keys()):
			if 'Armor Penetration' or 'Magic Penetration' in item_data[index]['tags']:
				print(item_data[index]['name'], ': ', item_data[index]['plaintext'])
				item_data.pop(index, None)
	else:
		return

# Switch statement for tags
switcher = {
	'Assassin': assassin,
	'Fighter': fighter,
	'Mage': mage,
	'Marksman': marksman,
	'Support': support,
	'Tank': tank
}

def init(enemy_tags):
	init_items()

	# sort dictionary in descending order
	sorted_tags = dict(sorted(enemy_tags.items(), key=operator.itemgetter(1), reverse=True))
	
	print(sorted_tags)

	# while sorted_tags is not empty
	while sorted_tags:
		# get first tag in dictionary
		a_tag = str(list(sorted_tags.keys())[0])
		# pop it from dictionary
		sorted_tags.pop(a_tag)
		# get the function and execute it
		function = switcher.get(a_tag)
		function()